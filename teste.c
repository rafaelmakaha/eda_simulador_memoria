#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>

typedef struct processo{
    int tam;
    int duracao;
    int h_origem;
    char tipo;
    char* label;
    struct processo* prox;
    struct processo* ant;
}Processo;

typedef struct cab{
    int mem_total;
    int mem_livre;
    int num_proc;
    Processo* ini;
    Processo* fim;
}Cab;

Cab* iniciaCab(Cab* l){
  Processo* novo = (Processo*)malloc(sizeof(Processo));

  l = (Cab*)malloc(sizeof(Cab));
  l->mem_total = 6;
  l->mem_livre = l->mem_total;
  l->num_proc = 0;
  l->ini = l->fim = novo;

  novo->tipo = 'H';
  novo->tam = l->mem_total;
  novo->prox = novo;
  novo->ant = novo;

  return l;
}

void realocarMemoria(Cab* l){
    int i=0, flag=0;
    Processo* p = l->ini;
    Processo* y = p->prox;
    Processo* novo = (Processo*)malloc(sizeof(Processo));

    if(l->fim->tipo == 'H'){
      do{
        if(p->tipo == 'H'){
          if(p == l->ini && flag==0){
            p = y;
            y = y->prox;
            flag++;
            continue;
          }
          printf("%d ", ++i);

          p->ant->prox = p->prox;
          p->prox->ant = p->ant;
          l->mem_livre += p->tam;
          free(p);
          p = NULL;
        }
        p = y;
        y = y->prox;
      }while(p != l->ini);

      printf("\n");


    }else{
      novo->tipo = 'H';
      novo->tam = 0;
      l->fim->prox = novo;
      l->ini->ant = novo;
      novo->prox = l->ini;
      novo->ant = l->fim;
      l->fim = novo;
      do{
        if(p->tipo == 'H'){
          if(p == l->ini && flag==0){
            p = y;
            y = y->prox;
            flag++;
            continue;
          }

          p->ant->prox = p->prox;
          p->prox->ant = p->ant;
          l->mem_livre += p->tam;
          free(p);
          p = NULL;
        }
        p = y;
        y = y->prox;
      }while(p != l->fim);

      
    }
    if(flag){
      y = l->ini;
      l->fim->prox = y->prox;
      y->prox->ant = l->fim;
      l->ini = y->prox;
      l->mem_livre += y->tam;
      free(y);
    }
    l->fim->tam = l->mem_livre;
}



void escreverProcesso(Cab* l, Processo* aux){
  Processo* novo = (Processo*)malloc(sizeof(Processo));
  
  novo->tipo = aux->tipo;
  novo->label = (char*)malloc(sizeof(char) * strlen(aux->label));
  strcpy(novo->label, aux->label);
  novo->tam = aux->tam;
  novo->duracao = aux->duracao;
  if(l->num_proc == 0){
    l->ini = novo;
    l->fim->ant = novo;
    l->fim->prox = novo;
    novo->ant = l->fim;
    novo->prox = l->fim;
  }else{
    l->fim->ant->prox = novo;
    novo->ant = l->fim->ant;
    novo->prox = l->fim;
    l->fim->ant = novo;
    l->mem_livre -= novo->tam;
    if(novo->tipo == 'P')
      l->num_proc++;
  }
}


Cab* carrega_arquivo(Cab* l){
  FILE* f;
  int op;
  int lixo;
  int i;
  char* c;
  int mem_total;
  Processo* aux = (Processo*)malloc(sizeof(Processo));            
  aux->label = (char*)malloc(sizeof(char)*50);

  f = fopen("simulacao.txt","r");

  if(f == NULL){
    printf("Erro ao ler arquivo!\n");
    getchar();
    getchar();
  }else{
    fscanf(f, "%d\n", &mem_total);
    l = iniciaCab(l);
    l->mem_livre = mem_total;
      while (fscanf(f, "%c %s %d %d\n",&aux->tipo,aux->label, &aux->tam, &aux->duracao) != EOF) {
        escreverProcesso(l,aux);
    } /*fim do while*/
  }
/* ao final, fecha o arquivo */
  op = fclose(f);
  if(op){
    printf("Dados carregados com sucesso!\n");
    getchar();
    getchar();
  }
  //free(aux->label);
  //free(aux);
return l;
}




int main(){
      Cab* l;// = iniciaCab(l);
      /*
      Processo* novo1 = (Processo*)malloc(sizeof(Processo));
      Processo* novo2 = (Processo*)malloc(sizeof(Processo));
      Processo* novo3 = (Processo*)malloc(sizeof(Processo));
      Processo* novo4 = (Processo*)malloc(sizeof(Processo));
      Processo* novo5 = (Processo*)malloc(sizeof(Processo));
      Processo* novo6 = (Processo*)malloc(sizeof(Processo));

      l->fim = novo6;
      l->ini = novo1;

      novo1->tipo = 'H';
      novo1->ant = l->fim;
      novo1->prox = novo2;
      novo1->tam = 1;
      l->mem_livre -= novo1->tam;

      novo2->tipo = 'H';
      novo2->ant = novo1;
      novo2->prox = novo3;
      novo2->tam = 1;
      l->mem_livre -= novo2->tam;

      novo3->tipo = 'H';
      novo3->ant = novo2;
      novo3->prox = novo4;
      novo3->tam = 1;
      l->mem_livre -= novo3->tam;

      novo4->tipo = 'H';
      novo4->ant = novo3;
      novo4->prox = novo5;
      novo4->tam = 1;
      l->mem_livre -= novo4->tam;

      novo5->tipo = 'P';
      novo5->ant = novo4;
      novo5->prox = novo6;
      novo5->tam = 1;
      l->mem_livre -= novo5->tam;

      novo6->tipo = 'P';
      novo6->ant = novo5;
      novo6->prox = l->ini;
      novo6->tam = 1;
      l->mem_livre -= novo6->tam;

      l->fim->prox = l->ini;
      */
      carrega_arquivo(l);

      printf("\n memoria livre: %d\n", l->mem_livre);

      Processo* p = l->ini;      
      do{
        if(p->tipo == 'H')
          printf("H,%d ->", p->tam);
        if(p->tipo == 'P')
          printf("P,%d ->", p->tam);

        p = p->prox;
      }while(p != l->ini);

      printf("\n");

      realocarMemoria(l);

      p = l->ini;
      do{
        if(p->tipo == 'H')
          printf("H,%d ->",p->tam);
        if(p->tipo == 'P')
          printf("P,%d ->",p->tam);

        p = p->prox;
      }while(p != l->ini);
      printf("\n");

  }


